TODO:

 - [x] Add the function of tracking enemy ships for the player's ship
 - [ ] Add Game Over Screen
 - [ ] Add background music
 - [ ] Add some SFX for shooting, player moving, enemy death e.t.c.
 - [ ] Add animations when destroying objects
